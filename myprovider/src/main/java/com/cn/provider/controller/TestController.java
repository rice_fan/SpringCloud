package com.cn.provider.controller;

import com.cn.provider.bean.Customer;
import com.cn.provider.dao.CustomerMapper;
import com.cn.provider.exception.RiceException;
import helper.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @Value("${server.port}")
    String port;
    @Autowired
    private CustomerMapper customerMapper;

    @GetMapping("/hiProvider/{custId}")
    public String home(@PathVariable Integer custId){
        Customer customer = customerMapper.selectByPrimaryKey(custId);
        return null == customer ? "对象不存在":"hi " + customer.getPhone() + ",i am from port:" + port;
    }
    @GetMapping("/result")
    public Result result(){
        System.out.println("11111111111111111111111111111111111111111111111111111");
        return Result.setSuccess("ok");
    }
}
