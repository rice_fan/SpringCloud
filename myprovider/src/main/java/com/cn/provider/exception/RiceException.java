package com.cn.provider.exception;


import helper.EnumTool;

/**
 * Created by rice on 2018/6/5.
 */
public class RiceException extends RuntimeException {

    private Integer code;
    private String message;

    public RiceException(String message) {
        this.code = EnumTool.RES_CODE.SELF_ERROR.getKey();//自定义异常
        this.message = message;
    }
    public RiceException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
