package com.cn.provider.exception;

import helper.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * controller 增强器
 *
 * @author rice
 * @since 2018/7/17
 */
@ControllerAdvice
public class MyControllerAdvice {
    private final static Logger logger = LoggerFactory.getLogger(MyControllerAdvice.class);

    /**
     * 全局异常捕捉处理
     *
     * @param ex
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Map errorHandler(HttpServletRequest request, Exception ex) {
        ex.printStackTrace();

        return Result.setError(404,null);
    }


    /**
     * 拦截捕捉自定义异常 RiceException.class
     *
     * @param ex
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = RiceException.class)
    public Map myErrorHandler(HttpServletRequest request, RiceException ex) {
        ex.printStackTrace();

        return Result.setError(ex.getCode(),ex.getMessage(),null);
    }

}