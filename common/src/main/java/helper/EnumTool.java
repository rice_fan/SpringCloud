package helper;

/**
 * Created by rice on 2018/6/5.
 */
public class EnumTool {

    public enum RES_CODE {
        /**
         * 错误码描述
         * 200：成功
         * 405：具体什么异常由抛出异常时指定
         * 408：具体哪个参数由抛出异常时指定
         */
        SUCCESS(200, "成功"),
        HALF_SUCCESS(201, "部分成功"),
        FAIL(400, "失败"),
        FWQ_ERROR(404, "服务器异常"),
        SELF_ERROR(417, "自定义异常"),
        TOKEN_ERROR(416, "无效的token"),
        RE_LOGIN(416, "用户不存在，请重新登录"),
        PARAM_NULL(417, "参数不能为空"),
        ;
        private Integer key;
        private String value;

        private RES_CODE(Integer key, String value) {
            this.key = key;
            this.value = value;
        }

        public static Integer getKeyByValue(String value) {
            for (RES_CODE v : RES_CODE.values()) {
                if (v.getValue().equals(value)) {
                    return v.getKey();
                }
            }
            return null;
        }

        public static String getValueByKey(Integer key) {
            for (RES_CODE v : RES_CODE.values()) {
                if (v.getKey().equals(key)) {
                    return v.getValue();
                }
            }
            return null;
        }

        public Integer getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }

}
