package helper;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 统一返回对象包装
 */
public class Result extends HashMap {

    private Result() {
    }



    public static Result setError() {
        Result result = new Result();
        result.put("message", EnumTool.RES_CODE.FWQ_ERROR.getValue());
        result.put("code", EnumTool.RES_CODE.FWQ_ERROR.getKey());
        return result;
    }

    public static Result setError(int code, String message, Long exceptionId) {
        Result result = new Result();
        result.put("message", message);
        result.put("code", code);
        result.put("exceptionId", exceptionId);
        return result;
    }

    public static Result setErrorWithException(int code, Long exceptionId) {
        Result result = new Result();
        result.put("message", EnumTool.RES_CODE.getValueByKey(code));
        result.put("code", code);
        result.put("exceptionId", exceptionId);
        //        result.put("times", CalcTimeHelper.getConsumeTime() + "ms");
        return result;
    }

    public static Result setError(int code, Object data) {
        Result result = new Result();
        result.put("message", EnumTool.RES_CODE.getValueByKey(code));
        result.put("code", code);
        result.put("data", data);
        return result;
    }

    public static Result setError(Object data) {
        Result result = new Result();
        EnumTool.RES_CODE fail = EnumTool.RES_CODE.FAIL;
        result.put("message", fail.getValue());
        result.put("code", fail.getKey());
        result.put("data", data);
        return result;
    }

    public static Result setError(String msg) {
        Result result = new Result();
        EnumTool.RES_CODE selfError = EnumTool.RES_CODE.SELF_ERROR;
        result.put("message", msg);
        result.put("code", selfError.getKey());
        return result;
    }

    public static Result setSuccess(int code, String message, Object data) {
        Result result = new Result();
        if (StringUtils.isNotBlank(message)) {
            result.put("message", message);
        } else {
            result.put("message", "成功");
        }
        result.put("code", code);
        if (data != null) {
            result.put("data", data);
        }
        return result;
    }

    public static Result setSuccess(int code, Object data) {
        Result result = new Result();
        result.put("message", EnumTool.RES_CODE.getValueByKey(code));
        result.put("code", code);
        result.put("data", data);
        return result;
    }

    public static Result setSuccess(Object data) {
        Result result = new Result();
        int code = 200;
        result.put("message", EnumTool.RES_CODE.getValueByKey(code));
        result.put("code", code);
        result.put("data", data);
        return result;
    }

    public static Result setSuccess() {
        Result result = new Result();
        int code = 200;
        result.put("message", EnumTool.RES_CODE.getValueByKey(code));
        result.put("code", code);
        return result;
    }

    public static <T> Result setSuccess(PageInfo<T> pageList) {
        Result result = new Result();
        int code = 200;
        result.put("message", EnumTool.RES_CODE.getValueByKey(code));
        result.put("code", code);
        Map pageMap = Maps.newHashMap();
        pageMap.put("rows", pageList.getList());//数据集
        pageMap.put("pageNum", pageList.getPageNum());//第几页
        pageMap.put("pages", pageList.getPages());//总页数
        pageMap.put("size", pageList.getSize());//当前页的数据长度
        pageMap.put("total", pageList.getTotal());//总数据量
        result.put("data", pageMap);
        return result;
    }

    public static <T> Result setSuccess(PageInfo<T> pageList, Object object) {
        Result result = new Result();
        int code = 200;
        result.put("message", EnumTool.RES_CODE.getValueByKey(code));
        result.put("code", code);
        //        result.put("times", CalcTimeHelper.getConsumeTime() + "ms");
        Map pageMap = Maps.newHashMap();
        pageMap.put("rows", pageList.getList());//数据集
        pageMap.put("pageNum", pageList.getPageNum());//第几页
        pageMap.put("pages", pageList.getPages());//总页数
        pageMap.put("size", pageList.getSize());//当前页的数据长度
        pageMap.put("total", pageList.getTotal());//总数据量
        result.put("data", pageMap);
        result.put("subData", object);//附属参数
        return result;
    }
}
