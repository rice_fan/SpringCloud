package helper;

public class CalcTimeHelper {
    public static ThreadLocal<Long> threadLocalMap = new ThreadLocal();

    public static void initTime() {
        threadLocalMap.set(System.currentTimeMillis());
    }

    public static Long getConsumeTime() {
        Long times;//耗时 ms
        try {
            times = System.currentTimeMillis() - threadLocalMap.get();
        } finally {
            threadLocalMap.remove();
        }
        return times == null ? 0 : times;
    }

}
