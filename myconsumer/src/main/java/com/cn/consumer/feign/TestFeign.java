package com.cn.consumer.feign;

import com.cn.consumer.config.MyFeignConfig;
import feign.Param;
import feign.RequestLine;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 之所以使用 Feign
 * 1. 因为他包含ribbon
 * 2. feign自身是一个声明式的伪http客户端，写起来更加思路清晰和方便
 * 3. fegn是一个采用基于接口的注解的编程方i式，更加简便
 */
@FeignClient(value = "rice-provider", configuration = MyFeignConfig.class, fallbackFactory = HystrixClientFallbackFactory.class) //value表示需要提供服务的应用名
public interface TestFeign {

    @RequestLine("GET /test/hiProvider/{custId}")//如果使用了 自定义的config  则必须使用@RequestLine
    public String homeTest(@Param("custId") Integer custId);

    @RequestLine("GET /test/result")
    public String result();
}


@Component
class HystrixClientFallbackFactory implements FallbackFactory<TestFeign> {
    @Override
    public TestFeign create(Throwable throwable) {
        return new TestFeign(){
            @Override
            public String homeTest(Integer custId) {
                return "sorry 尊敬的用户，服务暂不可用";
            }

            @Override
            public String result() {
                return "sorry 尊敬的用户，服务暂不可用";
            }
        };
    }
}