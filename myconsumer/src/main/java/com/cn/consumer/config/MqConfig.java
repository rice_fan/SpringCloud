package com.cn.consumer.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MqConfig {

    @Value("${spring.rabbitmq.custom.testQueue}")
    private String testQueue;
    @Value("${spring.rabbitmq.custom.testExchange}")
    private String testExchange;

    public String getTestExchange() {
        return testExchange;
    }

    public String getTestQueue() {
        return testQueue;
    }
}
