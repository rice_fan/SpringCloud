package com.cn.consumer.config;

import feign.Contract;
import feign.Request;
import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author fan_f
 * @create 2018-11-06 16:03
 * @desc
 **/
@Configuration
public class MyFeignConfig {

    public static int connectTimeOutMillis = 2000;//超时时间
    public static int readTimeOutMillis = 2000;

    @Bean
    public Contract feignContract(){

        return new feign.Contract.Default();
    }

//    @Bean
//    public Request.Options options() {
//        return new Request.Options(connectTimeOutMillis, readTimeOutMillis);
//    }
//
//    @Bean
//    public Retryer feignRetryer() {
//        return new Retryer.Default();
//    }

}
