package com.cn.consumer.controller;

import com.cn.consumer.config.MqConfig;
import com.cn.consumer.tool.KeyHelper;
import helper.Result;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/goods")
public class SaleGoodsController {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private MqConfig mqConfig;

    @GetMapping(value = "/miaoshao/{prdId}")
    public Result miaoShao(@PathVariable Long prdId){
        ListOperations list = redisTemplate.opsForList();
        Object o = list.leftPop(KeyHelper.goodsKey + prdId);
        if(o != null){
            System.out.println("有数据");
            amqpTemplate.convertAndSend(mqConfig.getTestQueue(),System.currentTimeMillis());
        }else{
            System.out.println("===null===");
        }
        return Result.setSuccess();
    }

    @GetMapping(value = "/cache/{prdId}")
    public Result cache(@PathVariable Long prdId){
        ListOperations list = redisTemplate.opsForList();
        for (int i = 0; i < 10; i++) {
            list.leftPush(KeyHelper.goodsKey + prdId, i);
        }

        return  Result.setSuccess();
    }

    @GetMapping(value = "/test/{textName}")
    public Result miaoShao(@PathVariable String textName){
        amqpTemplate.convertAndSend(mqConfig.getTestQueue(),textName);
        return  Result.setSuccess();
    }
}
