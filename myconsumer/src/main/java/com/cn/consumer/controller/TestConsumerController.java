package com.cn.consumer.controller;

import com.cn.consumer.feign.TestFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestConsumerController {

    @Autowired
    private TestFeign testFeign;

    @GetMapping(value = "/hi/home/{custId}")
    public String sayHi(@PathVariable Integer custId){
        return testFeign.homeTest(custId);
    }
    @GetMapping(value = "/result")
    public String result(){
        String result = testFeign.result();
        System.out.println(result);

        return result;
    }



}
