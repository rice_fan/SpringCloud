package com.rice.zuul;

import com.rice.zuul.back.MyFallbackProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@EnableZuulProxy
@SpringBootApplication
public class MyZuulApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(MyZuulApplication.class, args);

    }

    @Bean
    public MyFallbackProvider myFallbackProvider() {
        return new MyFallbackProvider();
    }
}
